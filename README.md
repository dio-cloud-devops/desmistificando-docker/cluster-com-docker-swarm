# CRIANDO CLUSTER DO DOCKER SWARN - local;
Instalar o vagrant: `pamac install vagrant` <br>

apos instalar executar: `vagrant init`, será criando um arquivo de configuração, e depois vc dever rodar com o comando: `vagrant up`

para acessa a maquina remota digite `vagrant ssh` ou acessa via ssh `ssh vagrant@ip` o usuario e senha é `vagrant`
---

* Para destruir a maquina usar-se `vagrant destroy -f` 
---
### CRIANDO 4 NODES
criando um diretorio vagrant_04host e nele criar os arquivos `Vagrantfile` e `instalar-docker.sh`e veja as configurações do mesmo dentro de cada arquivo e logo apos dentro do diretorio executar: `vagrant ip`<br>

Para acessar o nó faça assim: `vagrant ssh nomeDoHost` exemplo: `vagrant ssh node01` <br>
Vamos criar agora um nó gerente, com o comnado `docker swarm init --advertise-addr 192.168.3.30`, esse ip é o ip que vc receber do roteador ou ip publico. ao roda o comando ele ti mostrar uma saida de comando pareceido com este `docker swarm join --token SWMTKN-1-1s27i8wr36w4fc226aa983y8ao3143a5mfpr7qg8p99h50cql1-63bgs8n46szbclglcsgkph54l 192.168.3.30:2377` copie a saida do comando e rode nos outros nos, em todos os nos coler com o usuario root.

depois no Nó gerente [`node01`] rode o comando para sabe se os outros 3 nos estao como work. `docker node ls`<br>

### Trabalhando com serviços 
---
Listando os serviços no docker `docker serve ls`, criando um serviço com 8 replicas: `docker service create --name webs-server --replicas 8 -p 80:80 httpd`<br>
lista todos os serviços das replicas `docker service ps webs-server`.

Agora como ignora o node01 receber container? <br>
`docker node update --availability drain node01` para verificar `docker node update --availability drain node01` com isso o node01 irá para se fazer a replicar e as replicas que ele estava fazendo sera distribuido para os outros nó automaticamente.

para matar os serviços: `docker service rm webs-server` <br>
Para quer o node01 volte a fazer a replicar deve habilitar ele `docker node update --availability active node01`<br>

* Agora iremo fazer a replicação do dados que estao nos cluster.
Criar um volume antes: `docker volume create app`, caminho do volume e aqui iremos criar um documento. `index.html` para teste.
Estamos fazendo isso no nó *node01*, `docker service create --name meu-app --replicas 8 -dt -p 80:80 --mount type=volume,src=app,dst=/usr/local/apache2/htdocs/ httpd` veificando, `docker service ls` e `docker service ps meu-app`<br>

* criando um serviço de nfs para replicação.
`apt install nfs-server -y` isntalando no nó gerente, precisamos replicar o volume `/var/lib/docker/volumes/app/_data` este caminho.<br>
cole este caminho no final no arquivo `vim /etc/exports` com esta informações `/var/lib/docker/volumes/app/_data *(rw,sync,subtree_check)`<br>
execute este comando no terminal para fazer efeito a configuração: `exportfs -ar` e nas maquina clientes -works- precisar instlar o `apt install nfs-common` tem que fazer isso em todas work.
Logo apos roda o seguinte comando no nó work para ver se encotra  pasta: `showmount -e ipNode01`, em *cloud precidara liberar todas as portas udp* 

Agora para montar faça: `mount 192.168.3.34:/var/lib/docker/volumes/app/_data /var/lib/docker/volumes/app/_data` deve fazer isso e todas as maquina work. Agora so pegar o ip de cada nó e fazer o teste pelo navegador.

Lembrando para remove os nós faça `docker service rm meu-app` mais lembre de fazer isso no nò **Manager**

### Criando um balancemento de carga de container.
---

FEITO NO NÓ MANAGER.
`docker volume create datadb` criando um volume para o banco de dados.
Criando um novo container do banco de dado: `docker run -e MYSQL_ROOT_PASSWORD=Senha123 -e MYSQL_DATABASE=meubanco --name mysql-A -d -p 3306:3306 --mount type=volume,src=datadb,dst=/var/lib/mysql mysql:5.7` 

Acessando o banco e criando a tabela no banco, link <https://github.com/denilsonbonatti/docker-app-cluster><br>

depois va no volume que esta o documentos do httpd e crier o arquivo da aplicação php, no link acima.
Agora vamos montar o serviços, já que os outros nos foram removidos.
`docker service create --name meu-app --replicas 9 -dt -p 80:80 --mount type=volume,src=app,dst=/app/ webdevops/php-apache:alpine-php7`

========================

Diretorio dos arquivos:<br> 
https://academiapme-my.sharepoint.com/personal/kawan_dio_me/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fkawan%5Fdio%5Fme%2FDocuments%2FSlides%20dos%20Cursos%2FDocker%2FM%C3%B3dulo%203%2FCurso%201&ga=1

========================

https://web.dio.me/course/criando-um-cluster-com-o-docker-swarm/learning/d6daa112-56be-43e7-981b-b14512133f33?back=/track/cloud-devops-experience-banco-carrefour&tab=undefined&moduleId=undefined